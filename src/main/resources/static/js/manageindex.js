//这个文件用来获取用户信息
$(function(){

    getUserInfo()

    //退出功能，询问用户是否真的退出
    $('#btnLogout').on('click',function(){

        layer.confirm('是否退出登录？', {icon: 3, title:'提示'}, function(index){

//            localStorage.removeItem('token')

            location.href = '/managelogin.html'
            layui.layer.msg('退出成功!')

            layui.layer.close(index)
        })
    })

})

function getUserInfo(){

    axios({

        method:'GET',
        url:'/manager/getById',
        //headers用来设置请求头
        headers:{
            //如果本地没有token默认空字符串
            Authorization:localStorage.getItem('token')||''
        }
    }).then(function(res){

        console.log(res)
        renderName(res.data.data)

    },function(error){


    })
}

//渲染用户名
function renderName(userInfo){
    console.log(userInfo)

    var name =userInfo.username

    console.log(name)

    console.log(name[0].toUpperCase())

    var first=name[0].toUpperCase()

    $('.text-avatar').html(first)
}


//实现高亮方法
function highlight(kw){

    //移除所有背景色，判断dd中是否包含传入的kw关键字，就将dd高亮
    $('dd').removeClass('layui-this')

    $(`dd:contains(${kw})`).addClass('layui.this')
}