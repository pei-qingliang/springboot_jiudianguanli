package com.example.Service;

import com.example.Dao.UserRoom;

import java.util.List;

public interface UserRoomService {

    List<UserRoom> getAllUserRoom();
    List<UserRoom> pagetype(int strat,int pagesize);

    List<UserRoom> getAllLike(String rname);
    List<UserRoom> pagetypelike(int start,int pageSize,String rname);

    Boolean insertUserRoom(String roomtype,Integer roomID,String name,String IDcard,String phone);
    Boolean updateUserRoom(String roomtype,Integer roomID,String name,String IDcard,String phone);
    Boolean deleteUserRoom(Integer roomID);

}
