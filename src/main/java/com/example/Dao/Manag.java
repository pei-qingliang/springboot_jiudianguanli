package com.example.Dao;

public class Manag {

    private Integer id;
    private String managername;
    private String password;

    @Override
    public String toString() {
        return "Manag{" +
                "id=" + id +
                ", managername='" + managername + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getManagername() {
        return managername;
    }

    public void setManagername(String managername) {
        this.managername = managername;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
