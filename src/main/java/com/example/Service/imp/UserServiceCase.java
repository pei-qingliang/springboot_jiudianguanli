package com.example.Service.imp;

import com.example.Dao.User;
import com.example.Mapper.UserMapper;
import com.example.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceCase implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User getoneuser(String username) {
        return userMapper.getname(username);
    }

    @Override
    public User selectUser(String username,String password) {
        return userMapper.getById(username,password);
    }

    @Override
    public Boolean insertUser(String username,String password) {
        return userMapper.insert(username,password)>0;
    }

    @Override
    public Boolean updateUser(User user) {
        return userMapper.update(user)>0;
    }

    @Override
    public Boolean deleteUser(Integer id) {
        return userMapper.delete(id)>0;
    }

    @Override
    public List<User> getAllUser() {
        return userMapper.getAll();
    }

    @Override
    public List<User> queryUser(int a, int b) {
        int start=(a-1)*b;

        int pageSize=b;
        return userMapper.PageType(start,pageSize);
    }

    @Override
    public List<User> getLike(String rname) {
        return userMapper.getlike(rname);
    }

    @Override
    public List<User> pageLike(Integer a, Integer b, String rname) {
        int start=(a-1)*b;

        int pagesize=b;
        return userMapper.PageTypeLike(start,pagesize,rname);
    }


}
