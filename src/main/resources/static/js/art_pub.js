$(function () {


    var listParams = {
        start: 1,
        pagesize: 5,
        rname:'',
    }


    getCateList()


    function getCateList() {

        axios({

            method: 'GET',
            url: '/users/getAll',
            params: listParams
        }).then(function (res) {

            console.log(res.data)


            var list = []

            res.data.data.forEach(function (item) {
                list.push(`
                <tr>
                    <td>${item.id}</td>
                    <td>${item.username}</td>
                    <td>${item.password}</td>
                    <td>${item.name}</td>
                    <td>${item.IDcard}</td>
                    <td>${item.phone}</td>
                    <td>${item.sex}</td>
                    <td>${item.money}</td>
                    <td> 
                        <button type="button" id="btn-del" class="layui-btn layui-btn-danger layui-btn-sm" data-id="${item.id}">删除 </button>

                    </td>
                </tr> 
                `)
            })

            renderPage(res.data)

            $('tbody').html(list)
        })
    }


    function getLike() {

        axios({

            method: 'POST',
            url: '/users/getLike',
            params: listParams
        }).then(function (res) {

            console.log(res.data)


            var list = []

            res.data.data.forEach(function (item) {
                list.push(`
                <tr>
                    <td>${item.id}</td>
                    <td>${item.username}</td>
                    <td>${item.password}</td>
                    <td>${item.name}</td>
                    <td>${item.IDcard}</td>
                    <td>${item.phone}</td>
                    <td>${item.sex}</td>
                    <td>${item.money}</td>
                    <td> 
                        <button type="button" id="btn-del" class="layui-btn layui-btn-danger layui-btn-sm" data-id="${item.id}">删除 </button>

                    </td>
                </tr> 
                `)
            })

            renderPage(res.data)

            $('tbody').html(list)
        })
    }

    $('#form-search').on('submit', function (e) {
        e.preventDefault()
        // 获取表单中选中项的值
        var rname = $('#rname').val()
        // 为查询参数对象 listParams 中对应的属性赋值
        listParams.rname = rname
        layui.layer.msg('查询成功!')
        // 根据最新的筛选条件，重新渲染表格的数据
        getLike()
    })


    function renderPage(total) {

        layui.laypage.render({

            elem: "pagebox",  //将分页渲染到哪个区域，不需要 #
            count: total.total,          //数据总条数，需要去服务器获取
            limit: listParams.pagesize,  //每页显示条数
            curr: listParams.start,
            layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
            limits: [5, 10, 15, 20, 25],

            jump: function (obj, first) {
                //obj包含了当前分页的所有参数，比如：
                console.log(obj.curr) //得到当前页，以便向服务端请求对应页的数据。
                console.log(obj.limit)

                //将当前页码传给pagenum
                listParams.start = obj.curr

                //将每页显示条数赋值给pagesize，可以更改每页显示条数
                listParams.pagesize = obj.limit
                //重新获取文章列表数据
                //需要判断是否为首次加载，如果是，不能重复活的文章列表功能
                if (!first) (
                    getCateList()
                )

            }
        })

    }


    //删除文章功能
    $('tbody').on('click', '#btn-del', function () {

        var id = $(this).attr('data-id')

        var len = $('.btn-delete').length

        layer.confirm('确认删除?', {icon: 3, title: '提示'}, function (index) {

            axios({
                method: 'DELETE',
                url: '/users/delete',
                params: {id: id}

            }).then(function (res) {

                layui.layer.msg('删除房间成功!')
                if (len == 1) {
                    listParams.start = listParams.start == 1 ? 1 : listParams.start - 1
                }
                getCateList()
            })
            layer.close(index)
        })

    })
})