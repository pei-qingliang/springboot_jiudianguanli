package com.example.Service;

import com.example.Dao.User;

import java.util.List;

public interface UserService {

    User getoneuser(String username);
    User selectUser(String username,String password);
    Boolean insertUser(String username,String password);
    Boolean updateUser(User user);
    Boolean deleteUser(Integer id);
    List<User> getAllUser();
    List<User> queryUser(int pageNumber, int pageSize);
    List<User> getLike(String rname);
    List<User> pageLike(Integer start,Integer pageSize,String rname);
}
