package com.example.Mapper;


import com.example.Dao.User;
import com.example.Dao.UserRoom;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserRoomMapper {

    @Select("select * from userroom")
    List<UserRoom> getAll();

    @Select("select * from userroom where roomtype like '%${rname}%'")
    List<UserRoom> getlike(String rname);

    @Insert("insert into userroom(roomtype,roomID,name,IDcard,phone) values(#{roomtype},#{roomID},#{name},#{IDcard},#{phone})")
    Integer insert(String roomtype,Integer roomID,String name,String IDcard,String phone);

    @Delete("delete from userroom where roomID = #{roomID}")
    Integer delete(Integer roomID);

    @Update("update into userroom(roomtype,roomID,name,IDcard,phone) values(#{roomtype},#{roomID},#{name},#{IDcard},#{phone})")
    Integer update(String roomtype,Integer roomID,String name,String IDcard,String phone);


    //分页
    @Select("select * from userroom limit #{start},#{pageSize}")
    public List<UserRoom> PageType(@Param("start") int pageNumber, @Param("pageSize") int pageSize);

    //分页
    @Select("select * from userroom where roomtype like '%${rname}%' limit #{start},#{pageSize}")
    public List<UserRoom> PageTypeLike(int start,int pageSize,String rname);
}
