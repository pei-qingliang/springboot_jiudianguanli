package com.example.Dao;

public class Room {

    private Integer id;
    //房间类型，大床房什么的
    private String roomtype;
    //房间状态，被预约还是空闲
    private String state;
    //客户对房间的评论，待用
    private String word;
    //房间描述
    private String detail;
    //房间号
    private Integer roomID;
    //房间价格
    private Integer price;

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", roomtype='" + roomtype + '\'' +
                ", state=" + state +
                ", word='" + word + '\'' +
                ", detail='" + detail + '\'' +
                ", roomID=" + roomID +
                ", price=" + price +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoomtype() {
        return roomtype;
    }

    public void setRoomtype(String roomtype) {
        this.roomtype = roomtype;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Integer getRoomID() {
        return roomID;
    }

    public void setRoomID(Integer roomID) {
        this.roomID = roomID;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
