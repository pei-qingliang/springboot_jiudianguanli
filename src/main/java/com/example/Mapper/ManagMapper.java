package com.example.Mapper;

import com.example.Dao.Manag;
import com.example.Dao.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ManagMapper {

    @Select("select * from manag where managername = #{managername} AND password = #{password}")
    Manag getById(@Param("managername")String managername,@Param("password") String password);

    @Update("update manag set managername = #{managername},password = #{password} where id = #{id}")
    Integer update(Manag manag);

    @Select("select * from manag")
    List<Manag> getAll();
}
