package com.example.Controller;

import com.example.Dao.Room;

import com.example.Dao.UserRoom;
import com.example.Service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/room")
public class RoomController {

    @Autowired
    private RoomService roomService;

    @GetMapping("/getAll")
    public Map<String,Object> getAll(int start,int pagesize){
        List<Room> roomList=roomService.getAllRoom();
        List<Room> roomList1=roomService.pagetype(start,pagesize);
        Map<String,Object> roommap=new HashMap<>();

        roommap.put("data",roomList1);
        roommap.put("total",roomList.size());
        return roommap;
    }

    @PostMapping("/insert")
    public Boolean insert(Room room){

//        System.out.println(roomService.insertRoom(room));
        return roomService.insertRoom(room);
    }

    @PutMapping("/update")
    public Boolean update(Room room){

//        System.out.println(roomService.updateRoom(room));
        return roomService.updateRoom(room);
    }



    @DeleteMapping("/delete")
    public Boolean delete(Integer id){

//        System.out.println(roomService.deleteRoom(id));
        return roomService.deleteRoom(id);
    }

    @GetMapping("/getById")
    public Room getById(Integer id){

        System.out.println(roomService.selectRoom(id));
        return roomService.selectRoom(id);
    }

    @PostMapping("/getLike")
    public Map<String,Object> getLike(int start,int pagesize,String rname){
        List<Room> userRoomList=roomService.getAllLike(rname);
        List<Room> userRoomList1=roomService.pagetypelike(start,pagesize,rname);
        Map<String,Object> userroommap=new HashMap<>();
        userroommap.put("data",userRoomList1);
        userroommap.put("total",userRoomList.size());
        return userroommap;
    }
}
