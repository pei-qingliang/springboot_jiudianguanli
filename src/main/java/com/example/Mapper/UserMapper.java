package com.example.Mapper;

import com.example.Dao.Room;
import com.example.Dao.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {

    @Select("select * from user")
    List<User> getAll();

    //分页
    @Select("select * from user limit #{start},#{pageSize}")
    public List<User> PageType(@Param("start") int pageNumber,@Param("pageSize") int pageSize);

    @Select("select * from user where username = #{username}")
    User getname(@Param("username")String username);

    @Select("select * from user where username = #{username} AND password = #{password}")
    User getById(@Param("username")String username,@Param("password") String password);

    @Insert("insert into user(username,password) values(#{username},#{password})")
    Integer insert(@Param("username")String username,@Param("password") String password);

    @Delete("delete from user where id = #{id}")
    Integer delete(Integer id);

    @Update("update user set name = #{name},password = #{password},IDcard = #{IDcard},phone = #{phone} where id = #{id}")
    Integer update(User user);

    @Select("select * from user where name like '%${rname}%'")
    List<User> getlike(String rname);

    //分页
    @Select("select * from user where name like '%${rname}%' limit #{start},#{pageSize}")
    public List<User> PageTypeLike(int start,int pageSize,String rname);

}
