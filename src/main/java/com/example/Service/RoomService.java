package com.example.Service;

import com.example.Dao.Room;

import java.util.List;

public interface RoomService {

    Room selectRoom(Integer id);
    Boolean insertRoom(Room room);
    Boolean updateRoom(Room room);
    Boolean deleteRoom(Integer id);
    Boolean updatestate(String state,Integer roomID);
    Boolean updateidstate(String state,Integer id);
    List<Room> getAllRoom();
    List<Room> pagetype(Integer start,Integer pageSize);

    List<Room> getAllLike(String rname);
    List<Room> pagetypelike(int start,int pageSize,String rname);
}
