package com.example.Controller;


import com.example.Dao.User;
import com.example.Dao.UserRoom;
import com.example.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/getAll")
    public Map<String,Object> getAll(int start, int pagesize){
        List<User> userList=userService.getAllUser();

        List<User> userpage=userService.queryUser(start,pagesize);
        Map<String,Object> usermap=new HashMap<>();

        usermap.put("data",userpage);
        usermap.put("total",userList.size());

        System.out.println(usermap);
        return usermap;
    }



    @PostMapping("/insert")
    public Boolean insert(String username,String password){

        System.out.println(userService.insertUser(username,password));
        return userService.insertUser(username,password);
    }

    @PutMapping("/update")
    public Boolean update(User user){

        System.out.println(userService.updateUser(user));
        return userService.updateUser(user);
    }

    @DeleteMapping("/delete")
    public Boolean delete(Integer id){

        System.out.println(userService.deleteUser(id));
        return userService.deleteUser(id);
    }

    @GetMapping("/getById")
    public User getById(String username,String password){

        System.out.println(userService.selectUser(username,password));
        return userService.selectUser(username,password);
    }

    @GetMapping("/getone")
    public User getone(String username){

        System.out.println(userService.getoneuser(username));
        return userService.getoneuser(username);
    }

    @PostMapping("/getLike")
    public Map<String,Object> getLike(int start,int pagesize,String rname){
        List<User> userList=userService.getLike(rname);
        List<User> userList1=userService.pageLike(start,pagesize,rname);
        Map<String,Object> usermap=new HashMap<>();
        usermap.put("data",userList1);
        usermap.put("total",userList.size());
        return usermap;
    }




}
