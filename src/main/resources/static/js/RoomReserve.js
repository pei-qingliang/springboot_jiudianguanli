$(function () {


    var listParams = {
        start: 1,       //默认第几页
        pagesize: 5,      //每页几条数据
        rname:'',
                //文章发布状态，如果是"已发布"就是对应值，否则是草稿
    }


    getArticleList()



    function getArticleList() {
        axios({
            method: 'GET',
            url: '/room/getAll',
            params: listParams

        }).then(function (res){
            console.log(res)


            var list = []

            res.data.data.forEach(function (item) {
                list.push(`
                <tr>
                    <th>${item.roomtype}</th>
                    <th>${item.state}</th>
                    <th>${item.roomID}</th>
                    <th>${item.word}</th>
                    <th>${item.detail}</th>
                    <th>${item.price}</th>
                    <th>
                        <button class="layui-btn" id="btn-res" data-id="${item.id}">预定</button>
                    </th>
                </tr>
                `)
            })

            renderPage(res.data)

            $('tbody').html(list)


        })
    }

    //模糊查询
    function getAllLike(){
        axios({
            method:'POST',
            url:'room/getLike',
            params: listParams
        }).then(function (res){
            console.log(res)


            var list = []

            res.data.data.forEach(function (item) {
                list.push(`
                <tr>
                    <th>${item.roomtype}</th>
                    <th>${item.state}</th>
                    <th>${item.roomID}</th>
                    <th>${item.word}</th>
                    <th>${item.detail}</th>
                    <th>${item.price}</th>
                    <th>
                        <button class="layui-btn" id="btn-res" data-id="${item.id}">预定</button>
                    </th>
                </tr>
                `)
            })

            renderPage(res.data)

            $('tbody').html(list)

        })
    }

    $('#form-search').on('submit', function (e) {
        e.preventDefault()

        // 获取表单中选中项的值
        var rname = $('#rname').val()


        // 为查询参数对象 listParams 中对应的属性赋值
        listParams.rname=rname
        layui.layer.msg('查询成功!')

        // 根据最新的筛选条件，重新渲染表格的数据
       getAllLike()
    })



    //分页功能
    function renderPage(total) {

        layui.laypage.render({
            elem: "pagebox",  //将分页渲染到哪个区域，不需要 #
            count: total.total,          //数据总条数，需要去服务器获取
            limit: listParams.pagesize,  //每页显示条数
            curr: listParams.start,
            layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
            limits: [5, 10, 15, 20, 25],

            jump: function (obj, first) {
                //obj包含了当前分页的所有参数，比如：
                console.log(obj.curr) //得到当前页，以便向服务端请求对应页的数据。
                console.log(obj.limit)

                //将当前页码传给pagenum
                listParams.start = obj.curr

                //将每页显示条数赋值给pagesize，可以更改每页显示条数
                listParams.pagesize = obj.limit
                //重新获取文章列表数据
                //需要判断是否为首次加载，如果是，不能重复活的文章列表功能
                if (!first) (
                    getArticleList()
                )

            }
        })

    }



    var editIndex
    //预定功能
    $('tbody').on('click', '#btn-res', function () {

        var id = $(this).attr('data-id')

        axios({
            method:'GET',
            url:'/room/getById',
            params:{
                id:id
            }
        }).then(function(res){

            //将原来的两个数据放到input里面
            layui.form.val('form-edit',res.data)
        })

        console.log(id)

        editIndex = layui.layer.open({
            type:1,
            area:['500px','400px'],
            title:'编辑房间信息',
            content:$('#dialog-edit').html()
        })



    })


    $('body').on('submit','#form-edit',function(e){

        e.preventDefault()

        axios({

            method:'POST',
            url:'/userroom/insertmessage',
            data:$(this).serialize()
        }).then(function(res){

            layui.layer.msg('预定成功!')

            layui.layer.close(editIndex)

            getArticleList()

        })
    })





})