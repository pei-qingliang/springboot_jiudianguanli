package com.example.Config;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RespBean {
    private long code;
    private String message;
    private Object obj;


    //成功返回的结果
    public static RespBean success(String message){

        return new RespBean(200,message,null);
    }

    //重写成功返回的结果（带有对象）
    public static RespBean success(String message,Object obj){

        return new RespBean(200,message,obj);
    }

    //失败返回的结果
    public static RespBean error(String message){

        return new RespBean(500,message,null);
    }

    //重写失败返回的结果
    public static RespBean error(String message,Object obj){

        return new RespBean(500,message,obj);
    }

}
