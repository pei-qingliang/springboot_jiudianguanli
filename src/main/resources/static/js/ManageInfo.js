$(function(){

    initUserInfo()

    $('#btnReset').on('click',function(e){
        //阻止表单默认提交行为
        e.preventDefault()

        //重新获取用户的数据
        initUserInfo()

    })

    $('#updateUserInfo').on('submit',function(e){

        e.preventDefault()

        axios({

            method:'PUT',
            url:'/my/userinfo',
            data:$(this).serialize()

        }).then(function(res){



            layui.layer.msg(res.data.message)

            window.parent.getUserInfo()

        })


    })
})


function initUserInfo(){

    axios({

        method:'GET',

        url:'/my/userinfo'

    }).then(function(res){

        console.log(res)




        layui.form.val('updateUserInfo',res.data.data)
    })
}