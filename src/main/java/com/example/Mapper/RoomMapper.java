package com.example.Mapper;

import com.example.Dao.Room;
import com.example.Dao.User;
import com.example.Dao.UserRoom;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface RoomMapper {

    @Select("select * from room where id = #{id}")
    Room getById(Integer id);

    @Select("select * from room where roomtype like '%${rname}%'")
    List<Room> getlike(String rname);

    @Insert("insert into room(roomtype,state,word,detail,roomID,price) values(#{roomtype},#{state},#{word},#{detail},#{roomID},#{price})")
    Integer insert(Room room);

    @Delete("delete from room where id = #{id}")
    Integer delete(Integer id);

    @Update("update room set roomtype = #{roomtype},state = #{state},word = #{word},detail = #{detail},roomID = #{roomID},price = #{price} where id = #{id}")
    Integer update(Room room);

    @Update("update room set state = #{state} where roomID = #{roomID}")
    Integer updatestate(String state ,Integer roomID);

    @Update("update room set state = #{state} where id = #{id}")
    Integer updateidstate(String state ,Integer id);

    @Select("select * from room")
    List<Room> getAll();

    //分页
    @Select("select * from room limit #{start},#{pageSize}")
    public List<Room> PageType(@Param("start") int pageNumber,@Param("pageSize") int pageSize);

    //分页
    @Select("select * from room where roomtype like '%${rname}%' limit #{start},#{pageSize}")
    public List<Room> PageTypeLike(int start,int pageSize,String rname);
}
