package com.example.Controller;


import com.example.Dao.Manag;
import com.example.Service.ManagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/manager")
public class ManagerController {

    @Autowired
    private ManagService managService;

    @GetMapping("/getById")
    public Manag getById(String managername,String password){

        System.out.println(managService.selectManager(managername, password));
        return managService.selectManager(managername, password);
    }

    @PutMapping("/update")
    public Boolean update(Manag manag){

        System.out.println(managService.updateManager(manag));
        return managService.updateManager(manag);
    }

    @GetMapping("/getAll")
    public List<Manag> getAll(){

        System.out.println(managService.getAllManager());
        return managService.getAllManager();
    }

}
