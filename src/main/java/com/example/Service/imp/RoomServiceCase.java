package com.example.Service.imp;

import com.example.Dao.Room;
import com.example.Dao.UserRoom;
import com.example.Mapper.RoomMapper;
import com.example.Service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RoomServiceCase implements RoomService {

    @Autowired
    private RoomMapper roomMapper;

    @Override
    public List<Room> getAllLike(String rname) {
        return roomMapper.getlike(rname);
    }

    @Override
    public List<Room> pagetypelike(int a,int b,String rname) {
        int start=(a-1)*b;

        int pagesize=b;
        return roomMapper.PageTypeLike(start,pagesize,rname);
    }

    @Override
    public Room selectRoom(Integer id) {
        return roomMapper.getById(id);
    }

    @Override
    public Boolean insertRoom(Room room) {
        return roomMapper.insert(room)>0;
    }

    @Override
    public Boolean updateRoom(Room room) {
        return roomMapper.update(room)>0;
    }

    @Override
    public Boolean deleteRoom(Integer id) {
        return roomMapper.delete(id)>0;
    }

    @Override
    public Boolean updatestate(String state,Integer roomID) {
        return roomMapper.updatestate(state,roomID)>0;
    }

    @Override
    public Boolean updateidstate(String state, Integer id) {
        return roomMapper.updateidstate(state,id)>0;
    }

    @Override
    public List<Room> getAllRoom() {
        return roomMapper.getAll();
    }

    @Override
    public List<Room> pagetype(Integer a, Integer b) {
        int start=(a-1)*b;

        int pageSize=b;
        return roomMapper.PageType(start,pageSize);
    }
}
