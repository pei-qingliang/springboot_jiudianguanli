$(function(){


    var form = layui.form

    form.verify({
        username:[/^[a-zA-Z0-9]{6,12}$/,'用户名必须6-12位为数字或字母'],
        password:[/^\S{6,17}$/,'密码长度必须6-17位且不能有空格'],

        repwd : function(value){

            var pass =$('#password').val()

            if(value !==pass){
                return '两次输入的密码不一致!'
            }

        }
    })



    $('#form_login').on('submit',function(e){

        e.preventDefault()

        axios({
            method:'POST',
            url:'/manager/getById',
            data:$(this).serialize()
        }).then(function(res){
            console.log(res)

            layui.layer.msg('登录成功!')

            layui.layer.msg(res.data.message)

            localStorage.setItem('token',res.data.token)

            location.href='/manageindex.html'
        })

    })

})