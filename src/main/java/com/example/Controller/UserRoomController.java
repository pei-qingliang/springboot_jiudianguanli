package com.example.Controller;

import com.example.Dao.UserRoom;
import com.example.Service.RoomService;
import com.example.Service.UserRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/userroom")
public class UserRoomController {
    @Autowired
    private UserRoomService userRoomService;
    @Autowired
    private RoomService roomService;


    @GetMapping("/getAll")
    public Map<String,Object> getAll(int start,int pagesize){
        List<UserRoom> userRoomList=userRoomService.getAllUserRoom();
        List<UserRoom> userRoomList1=userRoomService.pagetype(start,pagesize);
        Map<String,Object> userroommap=new HashMap<>();
        userroommap.put("data",userRoomList1);
        userroommap.put("total",userRoomList.size());
        return userroommap;
    }


    @PostMapping("/insertmessage")
    public Boolean insert(String roomtype,Integer roomID,String name,String IDcard,String phone){
        roomService.updatestate("房间已满",roomID);
        return userRoomService.insertUserRoom(roomtype,roomID,name,IDcard,phone);
    }

    @DeleteMapping("/deletemsg")
    public Boolean delete(Integer roomID){

        System.out.println(roomService.updatestate("空房间",roomID));
        return userRoomService.deleteUserRoom(roomID);
    }

    @PutMapping("/updatemsg")
    public Boolean update(String roomtype,Integer roomID,String name,String IDcard,String phone){
        return userRoomService.updateUserRoom(roomtype,roomID,name,IDcard,phone);
    }

    @PostMapping("/getLike")
    public Map<String,Object> getLike(int start,int pagesize,String rname){
        List<UserRoom> userRoomList=userRoomService.getAllLike(rname);
        List<UserRoom> userRoomList1=userRoomService.pagetypelike(start,pagesize,rname);
        Map<String,Object> userroommap=new HashMap<>();
        userroommap.put("data",userRoomList1);
        userroommap.put("total",userRoomList.size());
        return userroommap;
    }


}
