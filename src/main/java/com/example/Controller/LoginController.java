package com.example.Controller;


import com.example.Dao.Manag;
import com.example.Dao.User;
import com.example.Service.ManagService;
import com.example.Service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Slf4j
@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private ManagService managService;





    @RequestMapping("userlogin")
    public String userlogin() {

        return "userlogin";
    }

    @RequestMapping("managers")
    public String managerlogin() {

        return "managelogin";
    }

    @RequestMapping(value = "/loginIn", method = RequestMethod.POST)
    public String login(String username, String password) {
        User user = userService.selectUser(username, password);
        log.info("username:{}", username);
        log.info("password:{}", password);
        if (user != null) {


            return "userindex";
        } else {
            return "error";
        }
    }


    @RequestMapping(value = "/manager", method = RequestMethod.POST)
    public String manager(String managername,String password) {
        Manag manag=managService.selectManager(managername, password);
        log.info("managername:{}", managername);
        log.info("password:{}", password);
        if (manag != null) {
            return "manageindex";
        } else {
            return "error";
        }
    }




    @RequestMapping("userregister")
    public String disp() {
        return "userlogin";
    }

    //实现注册功能
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String signUp(String username, String password) {
        User user=userService.getoneuser(username);

        if(user==null){

            userService.insertUser(username, password);
            log.info("username:{}", username);
            log.info("password:{}", password);

        }

        return "userlogin";

    }
}