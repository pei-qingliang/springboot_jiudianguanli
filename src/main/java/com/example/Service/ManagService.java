package com.example.Service;

import com.example.Dao.Manag;

import java.util.List;


public interface ManagService {

    Manag selectManager(String managername,String password);
    Boolean updateManager(Manag manag);
    List<Manag> getAllManager();
}
