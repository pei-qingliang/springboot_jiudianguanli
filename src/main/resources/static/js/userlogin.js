$(function(){



    $('#link_reg').on('click',function(){
        $('.reg-box').show()
        $('.login-box').hide()
    })

    $('#link_login').on('click',function(){
        $('.login-box').show()
        $('.reg-box').hide()
    })


    var form = layui.form

    form.verify({
        username:[/^[a-zA-Z0-9]{6,11}$/,'用户名必须为数字或字母'],
        password:[/^\S{6,16}$/,'密码长度必须6-12位且不能有空格'],

        repwd : function(value){

            var pass =$('#password').val()

            if(value !==pass){
                return '两次输入的密码不一致!'
            }

        }
    })



})