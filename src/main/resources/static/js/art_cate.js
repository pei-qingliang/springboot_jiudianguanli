$(function(){

    var listParams = {
        start: 1,       //默认第几页
        pagesize: 5,      //每页几条数据
        rname:''
        //文章发布状态，如果是"已发布"就是对应值，否则是草稿
    }


    getCateList()

    function getCateList(){

        axios({

            method:'GET',
            url:'/userroom/getAll',
            params: listParams

        }).then(function(res){



            console.log(res)

            var list = []

            res.data.data.forEach(function(item){
                list.push(`
                <tr>
                    <td>${item.roomtype}</td>
                    <td>${item.roomID}</td>
                    <td>${item.name}</td>
                    <td>${item.idcard}</td>
                    <td>${item.phone}</td>
                    <td> 
                        <button type="button" id="btn-del" class="layui-btn layui-btn-danger layui-btn-sm" data-id="${item.roomID}">删除 </button>
                        
                       
                    </td>
                </tr> 
                `)

            })

            renderPage(res.data)

            $('tbody').html(list)

        })
    }

    //模糊查询
    function getAllLike(){
        axios({
            method:'POST',
            url:'userroom/getLike',
            params: listParams
        }).then(function (res){
            console.log(res)


            var list = []

            res.data.data.forEach(function (item) {
                list.push(`
                <tr>
                    <td>${item.roomtype}</td>
                    <td>${item.roomID}</td>
                    <td>${item.name}</td>
                    <td>${item.idcard}</td>
                    <td>${item.phone}</td>
                    <td> 
                        <button type="button" id="btn-del" class="layui-btn layui-btn-danger layui-btn-sm" data-id="${item.roomID}">删除 </button>
                    </td>
                </tr> 
                `)
            })

            renderPage(res.data)

            $('tbody').html(list)

        })
    }

    $('#form-search').on('submit', function (e) {
        e.preventDefault()

        // 获取表单中选中项的值
        var rname = $('#rname').val()

        layui.layer.msg('查询成功!')
        // 为查询参数对象 listParams 中对应的属性赋值
        listParams.rname=rname

        // 根据最新的筛选条件，重新渲染表格的数据
        getAllLike()
    })



    //分页功能
    function renderPage(total) {

        layui.laypage.render({
            elem: "pagebox",  //将分页渲染到哪个区域，不需要 #
            count: total.total,          //数据总条数，需要去服务器获取
            limit: listParams.pagesize,  //每页显示条数
            curr: listParams.start,
            layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
            limits: [5, 10, 15, 20, 25],

            jump: function (obj, first) {
                //obj包含了当前分页的所有参数，比如：
                console.log(obj.curr) //得到当前页，以便向服务端请求对应页的数据。
                console.log(obj.limit)

                //将当前页码传给pagenum
                listParams.start = obj.curr

                //将每页显示条数赋值给pagesize，可以更改每页显示条数
                listParams.pagesize = obj.limit
                //重新获取文章列表数据
                //需要判断是否为首次加载，如果是，不能重复活的文章列表功能
                if (!first) (
                    getCateList()
                )

            }
        })

    }







    var index

    $('#addCate').on('click',function(){



        index=layui.layer.open({

            type:1,

            area:['600px','400px'],

            title:'添加用户入住',

            content:$('#template-add').html()
        })
    })


    $('body').on('submit','#addCate_form',function(e){

        e.preventDefault()

        axios({
            method:'POST',

            url:'/userroom/insertmessage',

            data:$(this).serialize()
        }).then(function(res){
            console.log(res)



            layui.layer.msg('添加成功!')


            layui.layer.close(index)

            getCateList()
        })
    })






    $('tbody').on('click','#btn-del',function(){

        var roomID = $(this).attr('data-id')

        layer.confirm('是否删除该条数据?', {icon: 3, title:'提示'}, function(index){
            //如果用户点击，确定则删除

            axios({

                method:'DELETE',

                url:'/userroom/deletemsg',

                params: {
                    roomID:roomID
                }
            }).then(function(res){
                console.log(res.data.roomID)



                layui.layer.msg('删除成功!')

                getCateList()
            })

            layer.close(index);
        })
    })



})