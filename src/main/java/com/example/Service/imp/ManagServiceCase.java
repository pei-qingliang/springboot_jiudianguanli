package com.example.Service.imp;

import com.example.Dao.Manag;
import com.example.Mapper.ManagMapper;
import com.example.Service.ManagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ManagServiceCase implements ManagService {

    @Autowired
    private ManagMapper managMapper;

    @Override
    public Manag selectManager(String managername,String password) {
        return managMapper.getById(managername,password);
    }

    @Override
    public Boolean updateManager(Manag manag) {
        return managMapper.update(manag)>0;
    }

    @Override
    public List<Manag> getAllManager() {
        return managMapper.getAll();
    }
}
