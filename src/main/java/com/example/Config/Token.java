package com.example.Config;


import com.example.Service.SecretKey;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;

import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.time.LocalDateTime;
import java.util.Date;




@Component
public class Token {

    public static SecretKey generalKey(){
      byte[] encodeKey=  Base64.decode(Constant.jwtsecret);
      SecretKey key= (SecretKey) new SecretKeySpec(encodeKey,0,encodeKey.length,"AES");
      return key;
    }

    public static String createJET(final String id,final String subject,final Long expireMills){

        SignatureAlgorithm algorithm=SignatureAlgorithm.HS256;
        Date now=new Date();
        SecretKey key=generalKey();

        JwtBuilder builder=Jwts.builder()
                .setId(id)
                .setSubject(subject)
                .setIssuedAt(now)
                .signWith(algorithm,key);

        if (expireMills>=0){
            Long realexpireMills=System.currentTimeMillis() + expireMills;

            builder.setExpiration(new Date(realexpireMills));
        }

       return builder.compact();
    }


}
