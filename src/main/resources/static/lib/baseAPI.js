axios.defaults.baseURL='http://localhost:8080'


axios.interceptors.request.use(function (response) {

    console.log(response)
    // 在发送请求之前做些什么
    if (response.url.indexOf('/my') !== -1) {
        // 如果请求的 URL 地址中，包含 /my 则为当前的请求头添加 Authorization 字段
        response.headers.Authorization = localStorage.getItem('token')
    }



    return response

}, function (error) {
    // 对请求错误做些什么

    if(error.response.status==401){

        localStorage.removeItem('token')
        location.href = ''
    }

    return Promise.reject(error)
})