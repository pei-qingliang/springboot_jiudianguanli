package com.example.Service.imp;

import com.example.Dao.UserRoom;
import com.example.Mapper.UserRoomMapper;
import com.example.Service.UserRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.events.Event;

import java.util.List;

@Service
public class UserRoomServiceCase implements UserRoomService {

    @Autowired
    private UserRoomMapper userRoomMapper;

    @Override
    public List<UserRoom> getAllUserRoom() {
        return userRoomMapper.getAll();
    }

    @Override
    public List<UserRoom> pagetype(int a,int b) {
        int start=(a-1)*b;

        int pagesize=b;
        return userRoomMapper.PageType(start,pagesize);
    }

    @Override
    public List<UserRoom> getAllLike(String rname) {
        return userRoomMapper.getlike(rname);
    }

    @Override
    public List<UserRoom> pagetypelike(int a,int b,String rname) {
        int start=(a-1)*b;

        int pagesize=b;
        return userRoomMapper.PageTypeLike(start,pagesize,rname);
    }

    @Override
    public Boolean insertUserRoom(String roomtype, Integer roomID, String name, String IDcard, String phone) {
        return userRoomMapper.insert(roomtype,roomID,name,IDcard,phone)>0;
    }

    @Override
    public Boolean updateUserRoom(String roomtype,Integer roomID,  String name, String IDcard, String phone) {
        return userRoomMapper.update(roomtype,roomID,name, IDcard,phone)>0;
    }

    @Override
    public Boolean deleteUserRoom(Integer roomID) {
        return userRoomMapper.delete(roomID)>0;
    }


}
